<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>virtiofs - shared file system for virtual machines / Standalone usage</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="navbar">
      <a href="/">Home</a>
    </div>

<h1>Standalone virtiofs usage</h1>

This document describes how to set up virtiofs for standalone QEMU testing
without Kata Containers or libvirt.
In general it&apos;s easier to debug basic issues in this environment than inside Kata Containers.

<h2>Components</h2>

   The following components are required:
<ol type="a">
      <li>A Linux 5.4 or later guest kernel with virtiofs support</li>
      <li>QEMU 4.2.0 or later for built-in virtiofs support</li>
      <li>virtiofsd</li>
</ol>

   The instructions assume that you already have available a Linux guest
image to run under QEMU and a Linux host on which you can build and run
the components.

<h2>The guest kernel</h2>

  An appropriately configured Linux 5.4 or later kernel can be used for
  virtiofs. Many distributions enable virtiofs so it is not necessary to build
  from source. However, if you want to compile a custom kernel or modify the
  virtiofs.ko guest driver you need to build from source:

<pre>
      git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
</pre>

  <p>Configure, build and install this kernel inside your guest VM, ensuring
  that the following config options are selected:</p>

<pre>
      CONFIG_VIRTIO_FS
      CONFIG_FUSE_DAX   # optional
</pre>

  Build and install the kernel in the guest, on most distros this can be most
  easily achieved with the commandline:

<pre>
      make -j 8 &amp;&amp; make -j 8 modules &amp;&amp; make -j 8 modules_install &amp;&amp; make -j 8 install
</pre>

  Boot the guest and ensure it boots normally.

  <p>Note: An alternative is to build the kernel on the host and pass the kernel on the QEMU command line;
        although this can take some work to get initrd&apos;s to work right.
  </p>

<h2>Building QEMU</h2>

  <p>QEMU 4.2.0 or later has built-in virtiofs support. Normally it is not
  necessary to build QEMU from source in order to use virtiofs. If you wish to
  develop virtiofs then you may need to build QEMU from source in order to
  modify the vhost-user-fs device setup code although most of the host-side
  virtiofs code lives in virtiofsd instead of QEMU.</p>

  <p>On the host, download the QEMU tree by:</p>

<pre>
      git clone https://gitlab.com/qemu-project/qemu.git
</pre>

  Inside the checkout create a build directory, and from inside that build directory:

<pre>
        ../configure --prefix=$PWD --target-list=x86_64-softmmu
        make -j 8
</pre>

<h2>Building virtiofsd</h2>

  Usually your distribution's virtiofsd package is sufficient and you do not
  need to compile virtiofsd from source. If you wish to develop virtiofsd or
  test the latest code then you can build it as follows:

<pre>
        git clone https://gitlab.com/virtio-fs/virtiofsd.git
        cd virtiofsd
        cargo build --release
</pre>

  See virtiofsd's readme file for information on build dependencies and build
  options.

<h2>Running with virtiofs</h2>

  A shared directory for testing is needed, this can initially be empty, but it&apos;s useful
  if it contains a file that you can check from inside the guest; we assume that $TESTDIR
  points to it.

  <p>First start virtiofsd daemon:</p>

      Launch virtiofsd (its location may vary depending on whether you built it from source or which distribution you are using):

<pre>
        /usr/libexec/virtiofsd --socket-path=/tmp/vhostqemu -o source=$TESTDIR -o cache=always
</pre>

  The socket path will also need to be passed to QEMU below.

  <br>Now start QEMU, for virtiofs we need to add parameters
<ul>
<li>  to create the communications socket
<pre>
      -chardev socket,id=char0,path=/tmp/vhostqemu
</pre>
</li>

<li>   instantiate the device
<pre>
      -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=myfs
</pre>

  The tag name is arbitrary and must match the tag given in the guests mount command.
</li>

<li>
   force use of memory sharable with virtiofsd.
<pre>
      -m 4G -object memory-backend-file,id=mem,size=4G,mem-path=/dev/shm,share=on -numa node,memdev=mem
</pre>
</li>
</ul>

   Add all these options to your standard QEMU command line; note the &apos;-m&apos; option and values are replacements
   for the existing option to set the memory size.

   <br>A typical QEMU command line might be:

<pre>
   qemu-system-x86_64 -M pc -cpu host --enable-kvm -smp 2 \
     -m 4G -object memory-backend-file,id=mem,size=4G,mem-path=/dev/shm,share=on -numa node,memdev=mem \
     -chardev socket,id=char0,path=/tmp/vhostqemu -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=myfs \
     -chardev stdio,mux=on,id=mon -mon chardev=mon,mode=readline -device virtio-serial-pci -device virtconsole,chardev=mon -vga none -display none \
     -drive if=virtio,file=rootfsimage.qcow2
</pre>

   That assumes that &apos;rootfsimage.qcow2&apos; is the VM built with the modified kernel.

   Log into the guest as root, and issue the mount command:

<pre>
   mount -t virtiofs myfs /mnt
</pre>

<p>Note that Linux 4.19-based virtiofs kernels required a different mount syntax <tt>mount -t virtio_fs none /mnt -o tag=myfs,rootmode=040000,user_id=0,group_id=0</tt> instead.</p>

   The contents of the /mnt directory in the guest should now reflect the $TESTDIR on the host.


<h2>Enabling DAX</h2>

   DAX mapping allows the guest to directly access the file contents from the
   hosts caches and thus avoids duplication between the guest and host. This
   feature is not complete and only available when building from source.

   <br>A mapping area (&apos;cache&apos;) is shared between virtiofsd and QEMU; this size must be specified on the command
   lines for QEMU, the command line for virtiofsd is unchanged.

   <p>The device section of the qemu command line changes to:</p>

<pre>
     -device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=myfs,cache-size=2G
</pre>

   Inside the guest the mount command becomes:

<pre>
   mount -t virtiofs myfs /mnt -o dax
</pre>

<p>Note that Linux 4.19-based virtiofs kernels required a different mount syntax <tt>mount -t virtio_fs none /mnt -o tag=myfs,rootmode=040000,user_id=0,group_id=0,dax</tt> instead.</p>

   Note that the size of the &apos;cache&apos; used doesn&apos;t increase the host RAM used directly, since it&apos;s just a mapping
   area for files.

<div class="footer">
    <p>This website is published under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a> license. The source code is available <a href="https://gitlab.com/virtio-fs/virtio-fs.gitlab.io/">here</a>.</p>
</div>
</body>
</html>

